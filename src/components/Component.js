export default class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.children = children;
		this.attribute = attribute;
	}
	render() {
		let render = "<" + this.tagName;
		if (this.attribute) {
			render += " " + this.attribute.name + "='" + this.attribute.value + "'";
		}
		render = this.renderChildre(render);
		return render;
	}

	renderChildre(render) {
		if (this.children instanceof Array) {
			render += ">";
			this.children.forEach(child => {
				render += this.isCompo(child);
			});
			render += "</" + this.tagName + ">";
		} else {
			render += ((!this.children) ? " />" : (">" + this.isCompo(this.children)+ "</" + this.tagName + ">"));
		}
		return render;
	}

	isCompo(child){
		console.log(child);
		return (child instanceof Component)?child.render():child;
	}
}
