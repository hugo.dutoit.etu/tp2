import Component from './Component.js';

export default class Img extends Component {
	constructor(src) {
		super("img", { name: 'src', value: src });
	}
}
