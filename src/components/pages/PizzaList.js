import Component from "../Component.js"
import PizzaThumbnail from "../PizzaThumbnail.js"

export default class PizzaList extends Component{
    #pizzas
    set pizzas(value){
        this.children=value.map(d=>new PizzaThumbnail(d));
    }
    constructor(data) {
        super("section",{name:"class",value:"pizzaList"},data.map(d=>new PizzaThumbnail(d)));
    }
}